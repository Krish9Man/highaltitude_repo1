/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.analytics.DataStore;
import com.assignment5.analytics.AnalysisHelper;
import java.io.IOException;
import java.util.Map;

public class GateWay {

    DataReader orderReader;
    DataReader productReader;
    AnalysisHelper helper;

    public GateWay() throws IOException {
        productReader = new DataReader("./ProductCatalogue.csv");
        orderReader = new DataReader("./SalesData.csv");
        helper = new AnalysisHelper();
    }

    public static void main(String args[]) throws IOException {
        GateWay gateway = new GateWay();
        gateway.readData();
    }

    private void readData() throws IOException {
        String[] row;
        while ((row = productReader.getNextRow()) != null) {
            generateProduct(row);
        }
        while ((row = orderReader.getNextRow()) != null) {
            generateOrder(row);
        }
        runAnalysis();
    }

    private void generateProduct(String[] row) {
        int productId = Integer.parseInt(row[0]);
        int minPrice = Integer.parseInt(row[1]);
        int maxPrice = Integer.parseInt(row[2]);
        int targetPrice = Integer.parseInt(row[3]);
        Product p = new Product(productId, minPrice, maxPrice, targetPrice);
        DataStore.getInstance().getProduct().put(productId, p);

    }

    private void generateOrder(String[] row) {
        Item i = generateItem(row);
        
        int orderId = Integer.parseInt(row[0]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        
        Order o = new Order(orderId, salesId, customerId, i);
        DataStore.getInstance().getOrder().put(orderId, o);
    }

    private void generateCustomer(String[] row) {
        int customerId = Integer.parseInt(row[5]);
        
        Customer c = new Customer(customerId);
        DataStore.getInstance().getCustomer().put(customerId, c);

    }

    private Item generateItem(String[] row) {

        int itemId = Integer.parseInt(row[1]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        int productId = Integer.parseInt(row[2]);

        Item i = new Item(itemId, salesPrice, quantity, productId);
        DataStore.getInstance().getItem().put(itemId, i);
        return i;
    }

   

    private void runAnalysis() {
        helper.top3products();
        helper.top3Customers();
        helper.totalRevenue();
        helper.top3salesperson();
        helper.originalCatalogue();
        helper.modifyCatalogue();
    }
}
