/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class Flight {

    private String airlineName;
    private String flightNumber;
    private String departure;
    private String arrival;
    private int seats;
    private String layoverLocation;
    private int price;
    private LocalDate doj;// = LocalDate.of(2017, 1, 13);
    private LocalTime localTime2;// = LocalTime.of(4, 30, 45);
    private boolean status;


    public Flight(String airlineName, String flightNumber, String departure, String arrival, int seats, String layoverLocation, int price, LocalDate doj, LocalTime localTime2,boolean status) {
        this.airlineName = airlineName;
        this.flightNumber = flightNumber;
        this.departure = departure;
        this.arrival = arrival;
        this.seats = seats;
        this.layoverLocation = layoverLocation;
        this.price = price;
        this.doj = doj;
        this.localTime2 = localTime2;
        this.status = status;
    }
    
    
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getLayoverLocation() {
        return layoverLocation;
    }

    public void setLayoverLocation(String layoverLocation) {
        this.layoverLocation = layoverLocation;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getDoj() {
        return doj;
    }

    public void setDoj(LocalDate doj) {
        this.doj = doj;
    }

    public LocalTime getLocalTime2() {
        return localTime2;
    }

    public void setLocalTime2(LocalTime localTime2) {
        this.localTime2 = localTime2;
    }

}
