/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Users.AirlinerUser;
import java.util.ArrayList;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class AirlinerDirectory {
    private ArrayList <AirlinerUser> AirlinerDirectory;
    public AirlinerDirectory(){
        AirlinerDirectory = new ArrayList<AirlinerUser>();
        //String userName, String passWord, String name, String owner, String contactInfo, String officeLocation,int numberOfFlights
        AirlinerUser a1 = new AirlinerUser("BAadmin", "BAadmin", "British Airways", "Quuen", "8288282822", "London", 40);
        AirlinerUser a2 = new AirlinerUser("UAadmin", "UAadmin", "United Airways", "New York", "88282323222", "Manhattan", 40);
        AirlinerUser a3 = new AirlinerUser("EMadmin", "EMadmin", "Emirates", "Shaik", "8288282822", "Dubai", 40);
        AirlinerUser a4 = new AirlinerUser("AIadmin", "AIadmin", "Air India", "Delhi", "8288282822", "Delhi", 40);
        
    }

    public ArrayList<AirlinerUser> getAirlinerDirectory() {
        return AirlinerDirectory;
    }

    public void setAirlinerDirectory(ArrayList<AirlinerUser> AirlinerDirectory) {
        this.AirlinerDirectory = AirlinerDirectory;
    }
    public void addAirliner(AirlinerUser a){
        AirlinerDirectory.add(a);
    }
    public void removeAirliner(AirlinerUser a){
        AirlinerDirectory.remove(a);
    }
    
    
}
