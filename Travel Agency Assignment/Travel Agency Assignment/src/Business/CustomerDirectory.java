/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class CustomerDirectory {
    private ArrayList <Customer> CustomerDirectory;
    public CustomerDirectory(){
        
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return CustomerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> CustomerDirectory) {
        this.CustomerDirectory = CustomerDirectory;
    }
    public void addCust(Customer c){
        CustomerDirectory.add(c);
    }
    
   public void removeCust(Customer c){
       CustomerDirectory.remove(c);
   }
    
    
    
}
