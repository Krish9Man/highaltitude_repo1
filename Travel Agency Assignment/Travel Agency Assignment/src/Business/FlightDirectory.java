/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class FlightDirectory {
    private ArrayList <Flight> FlightDirectory;
    
    
    public FlightDirectory(){
        FlightDirectory = new ArrayList<Flight>();
        //String airlineName, String flightNumber, String departure, String arrival, int seats, String layoverLocation, int price, LocalDate doj, LocalTime localTime2,boolean status) {
        Flight f1 = new Flight("United Arilines","US1020","Boston","London",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f2 = new Flight("United Arilines","US1021","Boston","Dublin",109,"Chicago",320,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f3 = new Flight("United Arilines","US1022","Boston","Amesterdam",60,"No Layover",420,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f4 = new Flight("United Arilines","US1023","Boston","San Fransico",120,"No Layover",120,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f5 = new Flight("British Arilines","BA001","Boston","London",99,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f6 = new Flight("British Arilines","BA002","London","Boston",19,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f7 = new Flight("British Arilines","BA003","Switzerland","Chicago",20,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f8 = new Flight("British Arilines","BA004","London","Bombay",179,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f9 = new Flight("Emirates","EM221","Boston","Dubai",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f10 = new Flight("Emirates","EM222","Boston","Abu Dhabi",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f11 = new Flight("Emirates","EM233","Boston","Muscat",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f12 = new Flight("Emirates","EM244","Boston","Ajman",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f13 = new Flight("Air India","AI111","Boston","Mumbai",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f14 = new Flight("Air India","AI222","Boston","Hyderbad",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
        Flight f15 = new Flight("Air India","AI333","Boston","Chennai",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true); 
        Flight f16 = new Flight("Air India","AI444","Boston","Delhi",79,"No Layover",220,LocalDate.of(2017, 1, 13),LocalTime.now(),true);
    
    
    
    
    
    }
    
    public void addFlight(Flight f){
        FlightDirectory.add(f);
        
    }
    public void removeFlight(Flight f){
        FlightDirectory.remove(f);
    }

    public ArrayList<Flight> getFlightDirectory() {
        return FlightDirectory;
    }

    public void setFlightDirectory(ArrayList<Flight> FlightDirectory) {
        this.FlightDirectory = FlightDirectory;
    }

    
    
}
