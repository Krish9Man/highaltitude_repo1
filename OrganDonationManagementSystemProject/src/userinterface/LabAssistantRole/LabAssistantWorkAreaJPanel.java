/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.LabAssistantRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AwarnessEventManagementOrganization;
import Business.Organization.LabOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.EventWorkRequest;
import Business.WorkQueue.LabTestWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import userinterface.EventOrganizerRole.DisplayEventsJPanel;

/**
 *
 * @author raunak
 */
public class LabAssistantWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount userAccount;
    private LabOrganization labOrganization;

    /**
     * Creates new form LabAssistantWorkAreaJPanel
     */
    public LabAssistantWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, EcoSystem business) {
        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.business = business;
        this.labOrganization = (LabOrganization) organization;
        profilepic();
        username.setText(userAccount.getUsername());
        qualifications.setText(userAccount.getQualifications());

        populateTable();
        upcomingevents();

    }

    public void upcomingevents() {
        for (Network network : business.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    System.err.println(organization);
                    //organization.getWorkQueue().getWorkRequestList().add(request);

                    if (organization instanceof AwarnessEventManagementOrganization) {

                        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
                            if (request instanceof EventWorkRequest) {
                                if (jLabel1.getText().equals("No Upcoming Events")) {
                                    jLabel1.setText(request.getMessage());
                                } else if (jLabel2.getText().equals("No Upcoming Events")) {
                                    jLabel2.setText(request.getMessage());
                                } else if (jLabel3.getText().equals("No Upcoming Events")) {
                                    jLabel3.setText(request.getMessage());
                                }

                            }
                        }

                    }
                }
            }
        }

    }

    public void profilepic() {
        ImageIcon image_path = new ImageIcon(userAccount.getProfile());
        profilePicture.setIcon(image_path);

    }

    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : labOrganization.getWorkQueue().getWorkRequestList()) {
            System.out.println("LAb work queue " + request);
            Object[] row = new Object[6];
            //row[0] = request;
            row[1] = ((LabTestWorkRequest) request).getPatientAccount().getEmployee().getName();
            //row[1] = ((LabTestWorkRequest) request).getPatient_id();
            row[2] = request.getSender().getEmployee().getName();
            //request.getSender().getEmployee().getName();
            row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[4] = request.getStatus();
            row[0] = request;
            //row[5] = request;
            //row[6] = request.getMessage();
            model.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        refreshJButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        profilePicture = new javax.swing.JLabel();
        username = new javax.swing.JLabel();
        qualifications = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        lbluser = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        workRequestJTable.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Message", "Patient Name", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);
        if (workRequestJTable.getColumnModel().getColumnCount() > 0) {
            workRequestJTable.getColumnModel().getColumn(0).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(1).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(2).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(3).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(4).setResizable(false);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 180, 1420, 670));

        assignJButton.setBackground(new java.awt.Color(0, 51, 102));
        assignJButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        assignJButton.setForeground(new java.awt.Color(255, 255, 255));
        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });
        add(assignJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1430, 860, -1, -1));

        processJButton.setBackground(new java.awt.Color(0, 51, 102));
        processJButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        processJButton.setForeground(new java.awt.Color(255, 255, 255));
        processJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/16/Knob-Smart-icon.png"))); // NOI18N
        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });
        add(processJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1550, 860, -1, -1));

        refreshJButton.setBackground(new java.awt.Color(0, 51, 102));
        refreshJButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        refreshJButton.setForeground(new java.awt.Color(255, 255, 255));
        refreshJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/16/Knob-Refresh-icon.png"))); // NOI18N
        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });
        add(refreshJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1550, 140, -1, -1));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 40, 10, 990));

        profilePicture.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        profilePicture.setText("User profile Picture");
        add(profilePicture, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 180, 210));

        username.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        username.setText("User Name");
        add(username, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, 150, -1));

        qualifications.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        qualifications.setText("quals");
        add(qualifications, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, 160, -1));

        jLabel4.setFont(new java.awt.Font("Cambria", 3, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 51));
        jLabel4.setText("Upcoming Events");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, -1, -1));

        jLabel1.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel1.setText("No Upcoming Events");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, -1, -1));

        jLabel2.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel2.setText("No Upcoming Events");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 440, -1, -1));

        jLabel3.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel3.setText("No Upcoming Events");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, -1, 20));

        jLabel5.setFont(new java.awt.Font("Cambria", 1, 24)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("LAB ASSISTANT DASHBOARD");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 1640, -1));

        jButton1.setBackground(new java.awt.Color(0, 51, 102));
        jButton1.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/16/Knob-Info-icon.png"))); // NOI18N
        jButton1.setText("View Events");
        jButton1.setToolTipText("Click here to view events");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 520, 160, -1));

        lbluser.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        lbluser.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lbluser.setText("Username:");
        add(lbluser, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null,"Please select the request to assign");
            return;
        }

        WorkRequest request = (WorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        if(request.getStatus().equalsIgnoreCase("pending")){
            JOptionPane.showMessageDialog(null,"This request is under process");
        }
        else if(request.getStatus().equalsIgnoreCase("Sent")){
        request.setReceiver(userAccount);
        request.setStatus("Pending");
        populateTable();
        }
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null,"Please select the request to process");
            return;
        }

        LabTestWorkRequest request = (LabTestWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        if(request.getStatus().equalsIgnoreCase("completed")){
            JOptionPane.showMessageDialog(null,"This request has already been processed ");
        }   
        else if(request.getStatus().equalsIgnoreCase("sent")){
            JOptionPane.showMessageDialog(null,"Please assign the request to yourself first");
        }
        else if(request.getStatus().equalsIgnoreCase("pending") && !request.getReceiver().getEmployee().getName().equals(userAccount.getEmployee().getName())){
            JOptionPane.showMessageDialog(null,"This request has alreading been assigned to some other lab assistant");
        }
        else{
        request.setStatus("Processing");

        ProcessWorkRequestJPanel processWorkRequestJPanel = new ProcessWorkRequestJPanel(userProcessContainer, request);
        userProcessContainer.add("processWorkRequestJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        }
    }//GEN-LAST:event_processJButtonActionPerformed

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateTable();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if(jLabel1.getText().equals("No Upcoming Events")){
            JOptionPane.showMessageDialog(null,"Currently there are no events happening!!");
        }
        else{
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("DisplayEventsJPanel", new DisplayEventsJPanel(userProcessContainer,business));
        layout.next(userProcessContainer);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbluser;
    private javax.swing.JButton processJButton;
    private javax.swing.JLabel profilePicture;
    private javax.swing.JLabel qualifications;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JLabel username;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
