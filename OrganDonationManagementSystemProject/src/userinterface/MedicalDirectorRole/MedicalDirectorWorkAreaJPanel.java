/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.MedicalDirectorRole;

import Business.DB4OUtil.DB4OUtil;
import Business.Person.DonorDirectory;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AwarnessEventManagementOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.Organization.OrganTissueDonationOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.*;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import userinterface.DoctorRole.DoctorWorkAreaJPanel;
import userinterface.EventOrganizerRole.DisplayEventsJPanel;
import userinterface.LabAssistantRole.ProcessWorkRequestJPanel;

/**
 *
 * @author krish9
 */
public class MedicalDirectorWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount userAccount;
    private EcoSystem system;
    private OrganTissueDonationOrganization organTissueDonationOrganization;
    private DB4OUtil dB4OUtil = DB4OUtil.getInstance();
    private UserAccount docAcc;
    private DonorDirectory donorDirectory;
    private int s=0;
    private int f=0;

    /**
     * Creates new form MedicalDirectorWorkAreaJPanel
     */
    public MedicalDirectorWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, EcoSystem business, DonorDirectory donorDirectory) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.business = business;
        this.organTissueDonationOrganization = (OrganTissueDonationOrganization) organization;
        system = dB4OUtil.retrieveSystem();
        this.donorDirectory = donorDirectory;
        //System.out.print("Testing"+this.organTissueDonationOrganization);
        //pushwork();
        profileName.setText(userAccount.getUsername());
        quals.setText(userAccount.getQualifications());
        populateTable();
        profilepic();
        upcomingevents();
    }

    public void upcomingevents() {
        for (Network network : business.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    System.err.println(organization);
                    //organization.getWorkQueue().getWorkRequestList().add(request);

                    if (organization instanceof AwarnessEventManagementOrganization) {

                        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
                            if (request instanceof EventWorkRequest) {
                                if (jLabel7.getText().equals("No Upcoming Events")) {
                                    jLabel7.setText(request.getMessage());
                                } else if (jLabel8.getText().equals("No Upcoming Events")) {
                                    jLabel8.setText(request.getMessage());
                                } else if (jLabel9.getText().equals("No Upcoming Events")) {
                                    jLabel9.setText(request.getMessage());
                                }

                            }
                        }

                    }
                }
            }
        }

    }

    public void profilepic() {
        ImageIcon image_path = new ImageIcon(userAccount.getProfile());
        profilePicture.setIcon(image_path);

    }

    public void pushwork() {
        for (Network network : system.getNetworkList()) {

            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organizationd : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    System.out.println("checking for doctor organization" + organizationd);
                    if (organizationd instanceof DoctorOrganization) {
                        //organization.getWorkQueue().getWorkRequestList();
                        System.out.println("Inside pushwork");

                        for (WorkRequest request : organizationd.getWorkQueue().getWorkRequestList()) {

                            if (request instanceof TrailWorkRequest) {

                                organTissueDonationOrganization.getWorkQueue().getWorkRequestList().add(request);

                            }

                        }

                    }
                }
            }
        }
    }

    public void populateTable() {
       //System.out.println("Inside Medical Director "+organTissueDonationOrganization.getWorkQueue().getWorkRequestList());
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);
        // System.out.print("Testing"+this.organTissueDonationOrganization);
        for (WorkRequest request : organTissueDonationOrganization.getWorkQueue().getWorkRequestList()) {
            System.err.print("Inside workloop meddir" + request);
            Object[] row = new Object[6];
            row[0] = request;
            row[1] = request.getSender().getEmployee().getName();
            row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[3] = request.getStatus();
            row[4] = ((TrailWorkRequest) request).getPatient_name();
            row[5] = ((TrailWorkRequest) request).getSeverity();
            System.out.println("View"+((TrailWorkRequest) request).getMatchResult());
            viewAna.setEnabled(true);
            if(((TrailWorkRequest) request).getMatchResult() == null || ((TrailWorkRequest) request).getMatchResult().equals("")){
                viewAna.setEnabled(false);
            
            
          
            }
            
            else{
                    if(((TrailWorkRequest) request).getMatchResult().equalsIgnoreCase("Match Found")){
                s++;
                System.out.println(s);
            }
            if(((TrailWorkRequest) request).getMatchResult().equalsIgnoreCase("Match Not Found")){
                f++;
            } 
                 
            }
               viewAna.setEnabled(true);
            if(((TrailWorkRequest) request).getMatchResult() == null || ((TrailWorkRequest) request).getMatchResult().equals("")){
                viewAna.setEnabled(false);
            
            
          
            }
            
            else{
                    if(((TrailWorkRequest) request).getMatchResult().equalsIgnoreCase("Match Found")){
                s++;
                System.out.println(s);
            }
            if(((TrailWorkRequest) request).getMatchResult().equalsIgnoreCase("Match Not Found")){
                f++;
            } 
                 
            }
           
            model.addRow(row);
           
        }
    }
//    DefaultTableModel model = (DefaultTableModel)workRequestJTable.getModel();

    //model.setRowCount(0);
    // for(WorkRequest request : labOrganization.getWorkQueue().getWorkRequestList()){
//            Object[] row = new Object[4];
//            row[0] = request;
//            row[1] = request.getSender().getEmployee().getName();
//            row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
//            row[3] = request.getStatus();
//            
//            model.addRow(row);
//        }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        refreshJButton = new javax.swing.JButton();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        profilePicture = new javax.swing.JLabel();
        profileName = new javax.swing.JLabel();
        quals = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        viewAna = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        lbluser = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Cambria", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MEDICAL DIRECTOR DASHBOARD");

        refreshJButton.setBackground(new java.awt.Color(0, 51, 102));
        refreshJButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        refreshJButton.setForeground(new java.awt.Color(255, 255, 255));
        refreshJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/16/Knob-Refresh-icon.png"))); // NOI18N
        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });

        assignJButton.setBackground(new java.awt.Color(0, 51, 102));
        assignJButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        assignJButton.setForeground(new java.awt.Color(255, 255, 255));
        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });

        processJButton.setBackground(new java.awt.Color(0, 51, 102));
        processJButton.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        processJButton.setForeground(new java.awt.Color(255, 255, 255));
        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });

        profilePicture.setText("User Profile Picture");

        profileName.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        profileName.setText("UserName");

        quals.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        quals.setText("quals");

        jLabel6.setFont(new java.awt.Font("Cambria", 3, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 51));
        jLabel6.setText("Upcoming Events");

        jLabel7.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel7.setText("No Upcoming Events");

        jLabel8.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel8.setText("No Upcoming Events");

        jLabel9.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        jLabel9.setText("No Upcoming Events");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Message", "Sender", "Receiver", "Status", "PatientName", "Severity"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);

        viewAna.setBackground(new java.awt.Color(0, 51, 102));
        viewAna.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        viewAna.setForeground(new java.awt.Color(255, 255, 255));
        viewAna.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/16/Knob-Info-icon.png"))); // NOI18N
        viewAna.setText("View Analytics");
        viewAna.setToolTipText("Click here to view analysis");
        viewAna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewAnaActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(0, 51, 102));
        jButton1.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/16/Knob-Info-icon.png"))); // NOI18N
        jButton1.setText("View Events");
        jButton1.setToolTipText("Click here to view events");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lbluser.setFont(new java.awt.Font("Cambria", 0, 18)); // NOI18N
        lbluser.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lbluser.setText("Username:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(profileName)
                            .addComponent(quals)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(profilePicture, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(viewAna, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(lbluser))
                        .addGap(29, 29, 29)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1421, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(refreshJButton, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(assignJButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(processJButton)))))))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(refreshJButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 511, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(assignJButton)
                            .addComponent(processJButton))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(profilePicture, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(59, 59, 59)
                                .addComponent(lbluser)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(profileName)
                                .addGap(18, 18, 18)
                                .addComponent(quals)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addGap(33, 33, 33)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel9)
                                .addGap(53, 53, 53)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(viewAna)
                                .addContainerGap(429, Short.MAX_VALUE))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1003, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateTable();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        WorkRequest request = (WorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        if(request.getStatus().equalsIgnoreCase("sent")){
        request.setReceiver(userAccount);
        request.setStatus("Pending");
        populateTable();
         }
        else{
            JOptionPane.showMessageDialog(null, "This request is already being handled");
            return;
        }

    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed
        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        TrailWorkRequest request = (TrailWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        request.setStatus("Processing");
        request.setReceiver(userAccount);
//        OrganWorkRequest request = (OrganWorkRequest)workRequestJTable.getValueAt(selectedRow, 0);
//        
//     
//        request.setStatus("Processing");
        DirectorProcessWorkRequesJPanel directorProcessWorkRequestJPanel = new DirectorProcessWorkRequesJPanel(userProcessContainer, request, donorDirectory, business);
        userProcessContainer.add("processWorkRequestJPanel", directorProcessWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_processJButtonActionPerformed

    private void viewAnaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewAnaActionPerformed
        // TODO add your handling code here:
        DefaultPieDataset pieDataset = new DefaultPieDataset();
        System.out.println(s);
        System.out.println(f);
        pieDataset.setValue("Organ Matches", s);
        pieDataset.setValue("Organ Non Matches", f);
        JFreeChart chart  = ChartFactory.createPieChart("Pie Chart", pieDataset, true, true, true);
//        PiePlot3D P =(PiePlot3D)chart.getPlot();
      //  P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame = new ChartFrame("Pie Chart",chart);
        frame.setVisible(true);
        frame.setSize(450,500);
    }//GEN-LAST:event_viewAnaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if(jLabel7.getText().equals("No Upcoming Events")){
            JOptionPane.showMessageDialog(null,"Currently there are no events happening!!");
        }
        else{
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.add("DisplayEventsJPanel", new DisplayEventsJPanel(userProcessContainer,business));
        layout.next(userProcessContainer);}
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbluser;
    private javax.swing.JButton processJButton;
    private javax.swing.JLabel profileName;
    private javax.swing.JLabel profilePicture;
    private javax.swing.JLabel quals;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JButton viewAna;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
