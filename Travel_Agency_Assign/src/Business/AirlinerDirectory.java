/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Users.AirlinerUser;
import java.util.ArrayList;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class AirlinerDirectory {
    private ArrayList <AirlinerUser> airlinerDirectory;
    public AirlinerDirectory(){
        airlinerDirectory = new ArrayList<AirlinerUser>();
        //String userName, String passWord, String name, String owner, String contactInfo, String officeLocation,int numberOfFlights
        AirlinerUser a1 = new AirlinerUser("BAadmin", "BAadmin", "British Airlines", "Quuen", "8288282822", "London", 4);
        AirlinerUser a2 = new AirlinerUser("UAadmin", "UAadmin", "United Airlines", "New York", "88282323222", "Manhattan", 4);
        AirlinerUser a3 = new AirlinerUser("EMadmin", "EMadmin", "Emirates", "Shaik", "8288282822", "Dubai", 4);
        AirlinerUser a4 = new AirlinerUser("AIadmin", "AIadmin", "Air India", "Delhi", "8288282822", "Delhi", 4);
        
        airlinerDirectory.add(a1);
        airlinerDirectory.add(a2);
        airlinerDirectory.add(a3);
        airlinerDirectory.add(a4);
    }

    public ArrayList<AirlinerUser> getAirlinerDirectory() {
        return airlinerDirectory;
    }

    public void setAirlinerDirectory(ArrayList<AirlinerUser> airlinerDirectory) {
        this.airlinerDirectory = airlinerDirectory;
    }
    public void addAirliner(AirlinerUser a){
        airlinerDirectory.add(a);
    }
    public void removeAirliner(AirlinerUser a){
        airlinerDirectory.remove(a);
    }
    
    
}
