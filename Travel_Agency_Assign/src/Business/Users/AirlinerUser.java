/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class AirlinerUser {
    private String userName;
    private String passWord;
    private String name;
    private String owner;
    private String contactInfo;

    public int getNumberOfFlights() {
        return numberOfFlights;
    }

    public void setNumberOfFlights(int numberOfFlights) {
        this.numberOfFlights = numberOfFlights;
    }
    private String officeLocation;
    private int numberOfFlights;
    
    public AirlinerUser (){}

    public AirlinerUser(String userName, String passWord, String name, String owner, String contactInfo, String officeLocation,int numberOfFlights) {
        this.userName = userName;
        this.passWord = passWord;
        this.name = name;
        this.owner = owner;
        this.contactInfo = contactInfo;
        this.officeLocation = officeLocation;
        this.numberOfFlights=numberOfFlights;
    }
    
    public AirlinerUser(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getOfficeLocation() {
        return officeLocation;
    }

    public void setOfficeLocation(String officeLocation) {
        this.officeLocation = officeLocation;
    }

    public boolean verify(String password){
        if(password.equals(getPassWord()))
            return true;
        return false;
    }
    
    @Override
    public String toString(){
        return this.getName();
    }
    
}
