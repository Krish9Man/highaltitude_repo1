/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class FlightDirectory {

    private ArrayList<Flight> FlightDirectory;
    String calDay;
    String setDay;
    public FlightDirectory() {
        FlightDirectory = new ArrayList<Flight>();
        //LocalTime time3 = LocalTime.parse("12:32:22", 
           // DateTimeFormatter.ISO_TIME);
        //String airlineName, String flightNumber, String departure, String arrival, int seats, String layoverLocation, int price, LocalDate doj, LocalTime localTime2,boolean status) {
        Flight f1 = new Flight("United Airlines", "US1020", "Boston", "London", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.parse("17:32:22", DateTimeFormatter.ISO_TIME), true,"1A");
        Flight f2 = new Flight("United Airlines", "US1021", "Boston", "Dublin", 150, "Chicago", 320, LocalDate.of(2017, 1, 12), LocalTime.now(), true,"1A");
        Flight f3 = new Flight("United Airlines", "US1022", "Boston", "Amesterdam", 150, "No Layover", 420, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f4 = new Flight("United Airlines", "US1023", "Boston", "San Fransico", 150, "No Layover", 120, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f5 = new Flight("British Airlines", "BA001", "Boston", "London", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f6 = new Flight("British Airlines", "BA002", "London", "Boston", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f7 = new Flight("British Airlines", "BA003", "Switzerland", "Chicago", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f8 = new Flight("British Airlines", "BA004", "London", "Bombay", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f9 = new Flight("Emirates", "EM221", "Boston", "Dubai", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f10 = new Flight("Emirates", "EM222", "Boston", "Abu Dhabi", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f11 = new Flight("Emirates", "EM233", "Boston", "Muscat", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f12 = new Flight("Emirates", "EM244", "Boston", "Ajman", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f13 = new Flight("Air India", "AI111", "Boston", "Mumbai", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f14 = new Flight("Air India", "AI222", "Boston", "Hyderbad", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f15 = new Flight("Air India", "AI333", "Boston", "Chennai", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        Flight f16 = new Flight("Air India", "AI444", "Boston", "Delhi", 150, "No Layover", 220, LocalDate.of(2017, 1, 13), LocalTime.now(), true,"1A");
        System.out.print(f16.getLocalTime2());
        calDay =   f1.getLocalTime2().toString().substring(0,2);
        int time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f1.setWhattime(setDay);
        FlightDirectory.add(f1);
        
        
        calDay =   f2.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f2.setWhattime(setDay);
        FlightDirectory.add(f2);
        calDay =   f3.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f3.setWhattime(setDay);
        
        FlightDirectory.add(f3);
        calDay =   f4.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f4.setWhattime(setDay);
        FlightDirectory.add(f4);
        calDay =   f5.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f5.setWhattime(setDay);
        FlightDirectory.add(f5);
        calDay =   f6.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f6.setWhattime(setDay);
        FlightDirectory.add(f6);
        calDay =   f7.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f7.setWhattime(setDay);
        FlightDirectory.add(f7);
        calDay =   f8.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f8.setWhattime(setDay);
        FlightDirectory.add(f8);
        calDay =   f9.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f9.setWhattime(setDay);
        FlightDirectory.add(f9);
        calDay =   f10.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f10.setWhattime(setDay);
        FlightDirectory.add(f10);
        calDay =   f11.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f11.setWhattime(setDay);
        FlightDirectory.add(f11);
        calDay =   f12.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f12.setWhattime(setDay);
        FlightDirectory.add(f12);
        calDay =   f13.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f13.setWhattime(setDay);
        FlightDirectory.add(f13);
        calDay =   f14.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f14.setWhattime(setDay);
        FlightDirectory.add(f14);
        calDay =   f15.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f15.setWhattime(setDay);
        FlightDirectory.add(f15);
        calDay =   f16.getLocalTime2().toString().substring(0,2);
        time = Integer.parseInt(calDay);
        if(time>=5 && time<=15){
            setDay="morning";
        }
        else if(time>15 && time<=20){
        setDay="evening";
          }
        else{
            setDay="night";
        }
        
        f16.setWhattime(setDay);
        FlightDirectory.add(f16);

    }

    public void addFlight(Flight f) {
        FlightDirectory.add(f);

    }

    public void removeFlight(Flight f) {
        FlightDirectory.remove(f);
    }

    public ArrayList<Flight> getFlightDirectory() {
        return FlightDirectory;
    }

    public void setFlightDirectory(ArrayList<Flight> FlightDirectory) {
        this.FlightDirectory = FlightDirectory;
    }

}
