/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class Customer {

    private String fName;
    private String lName;
    private String gender;
    private String contactInfo;
    private int age;
    private String fromCity;
    private String toCity;
    private LocalDate doj;// = LocalDate.of(2017, 1, 13);
    //LocalTime localTime2;// = LocalTime.of(4, 30, 45);
    private String ticket;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Customer(String fName, String lName, String gender, String contactInfo, int age, String fromCity, String toCity, LocalDate doj) {
        this.fName = fName;
        this.lName = lName;
        this.gender = gender;
        this.contactInfo = contactInfo;
        this.age = age;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.doj = doj;
    }
    
    public Customer(){}

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public LocalDate getDoj() {
        return doj;
    }

    public void setDoj(LocalDate doj) {
        this.doj = doj;
    }

}
