/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class CustomerDirectory {
    private ArrayList <Customer> CustomerDirectory;
    public CustomerDirectory(){
        this.CustomerDirectory = new ArrayList<Customer>();
        //Customer c = new Customer();
        //String fName, String lName, String gender, String contactInfo, int age, String fromCity, String toCity, LocalDate doj
        Customer c = new Customer("Gopi","Krishna","Male","99292992",23,"boston","bfbf",LocalDate.now());
        CustomerDirectory.add(c);
        
        
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return CustomerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> CustomerDirectory) {
        this.CustomerDirectory = CustomerDirectory;
    }
    public void addCust(Customer c){
        CustomerDirectory.add(c);
    }
    
   public void removeCust(Customer c){
       CustomerDirectory.remove(c);
   }
    
    
    
}
