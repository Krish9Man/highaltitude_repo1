package Business;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gopikrishnamanchukonda
 */
public class OrderHistory {
    private ArrayList <Ticket> OrderHistory;
    public OrderHistory(){
        this.OrderHistory = new ArrayList<Ticket>();
        //String Name, String fromCity, String toCity, String Airliner, LocalDate date, LocalTime time, String seat,flightnumber
        Ticket t1 =  new Ticket("Gk","Boston","London","United Airlines",LocalDate.now(),LocalTime.now(),"A1","US1020");
        OrderHistory.add(t1);
        
    }
    public void addTicket(Ticket t){
        OrderHistory.add(t);
    }
    public void removeTicket(Ticket t){
        OrderHistory.remove(t);
    }

    public ArrayList<Ticket> getOrderHistory() {
        return OrderHistory;
    }

    public void setOrderHistory(ArrayList<Ticket> OrderHistory) {
        this.OrderHistory = OrderHistory;
    }
    
    
}
