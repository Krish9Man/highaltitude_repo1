/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class Ticket {
    private String Name;
    private String fromCity;
    private String toCity;
    private String Airliner;
    private LocalDate date;
    private LocalTime time;
    private String seat;
    private String flightNumber;
    private String ticketNumber;
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
public Ticket(){
    
}
  
public Ticket(String Name, String fromCity, String toCity, String Airliner, LocalDate date, LocalTime time, String seat,String flightNumber) {
        this.Name = Name;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.Airliner = Airliner;
        this.date = date;
        this.time = time;
        this.seat = seat;
        this.flightNumber = flightNumber;
    }
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getAirliner() {
        return Airliner;
    }

    public void setAirliner(String Airliner) {
        this.Airliner = Airliner;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    @Override
    public String toString(){
        return this.ticketNumber;
    }
    
    
    
}
