/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.AirlinerDirectory;
import Business.Users.AirlinerUser;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author gopikrishnamanchukonda
 */
public class RegisterAirJPanel extends javax.swing.JPanel {

      private AirlinerDirectory airlinerDirectory;
    private JPanel cardSequenceJPanel;
    //private JPanel createAirlJPanel;
    /**
     * Creates new form RegisterAirJPanel
     */
    public RegisterAirJPanel(JPanel cardSequenceJPanel, AirlinerDirectory airlinerDirectory) {
        initComponents();
         this.cardSequenceJPanel = cardSequenceJPanel;
        this.airlinerDirectory = airlinerDirectory;
        populateTable();
        txtAName.setEnabled(false);
            txtContact.setEnabled(false);
            txtNoofFlights.setEnabled(false);
            txtOfficeLoc.setEnabled(false);
            txtOwner.setEnabled(false);
    }
    
    void populateTable(){
        DefaultTableModel dtm = (DefaultTableModel)tblAirliner.getModel();
        dtm.setRowCount(0);
        for(AirlinerUser airline: airlinerDirectory.getAirlinerDirectory()){
        Object[] row = new Object[5];
        row[0] = airline;
        row[1] = airline.getOwner();
        row[2] = airline.getContactInfo();
        row[3] = airline.getNumberOfFlights();
        row[4] = airline.getOfficeLocation();
        dtm.addRow(row);
        
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblAirliner = new javax.swing.JTable();
        btnView = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        txtAName = new javax.swing.JTextField();
        txtOwner = new javax.swing.JTextField();
        txtContact = new javax.swing.JTextField();
        txtOfficeLoc = new javax.swing.JTextField();
        txtNoofFlights = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        tblAirliner.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "OwnerName", "ConactInfo", "numberOfFlights"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblAirliner);
        if (tblAirliner.getColumnModel().getColumnCount() > 0) {
            tblAirliner.getColumnModel().getColumn(0).setResizable(false);
            tblAirliner.getColumnModel().getColumn(1).setResizable(false);
            tblAirliner.getColumnModel().getColumn(2).setResizable(false);
            tblAirliner.getColumnModel().getColumn(3).setResizable(false);
        }

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnCreate.setText("Create New Airline");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        jLabel1.setText("Air LineName");

        jLabel2.setText("Owner Name");

        jLabel3.setText("Contact Info");

        jLabel4.setText("NumberOfFlights");

        jLabel7.setText("Office Location");

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jLabel5.setText("Register Airlines");

        jButton1.setText("< Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(368, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNoofFlights, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCreate)
                            .addComponent(txtContact, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnSave)
                        .addGap(357, 357, 357))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(txtOfficeLoc, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnUpdate)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnView))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel2)
                                            .addGap(33, 33, 33)
                                            .addComponent(txtOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtAName, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(123, 123, 123)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel3)))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jButton1)
                        .addGap(194, 194, 194)
                        .addComponent(jLabel5)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jButton1))
                        .addGap(28, 28, 28)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(btnCreate)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnView)
                    .addComponent(btnUpdate))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(txtAName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNoofFlights, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtContact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtOfficeLoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(btnSave)
                .addGap(168, 168, 168))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        // TODO add your handling code here:
        
        CardLayout layout = (CardLayout)cardSequenceJPanel.getLayout();
        cardSequenceJPanel.add(new CreateAirLineJPanel(cardSequenceJPanel,airlinerDirectory));
        layout.next(cardSequenceJPanel);
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblAirliner.getSelectedRow();
        if(selectedRow>=0){
            AirlinerUser airline = (AirlinerUser)tblAirliner.getValueAt(selectedRow,0);
            txtAName.setText(airline.getName());
            txtContact.setText(airline.getContactInfo());
            String numberOfFlights =  Integer.toString(airline.getNumberOfFlights());
            txtNoofFlights.setText(numberOfFlights);
            txtOfficeLoc.setText(airline.getOfficeLocation());
            txtOwner.setText(airline.getOwner());
            
            txtAName.setEnabled(true);
        txtContact.setEnabled(true);
        txtNoofFlights.setEnabled(true);
        txtOfficeLoc.setEnabled(true);
        txtOwner.setEnabled(true);
        }
        else{
            JOptionPane.showMessageDialog(null,"Please select any row");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
         if (txtAName.getText().equals("") || txtAName.getText() == null) {
            JOptionPane.showMessageDialog(null, "Airliner can't be empty");
            return;
        }
        
      
        if (!(txtAName.getText().matches("[a-zA-Z][a-zA-Z ]+") )) {
                JOptionPane.showMessageDialog(null, "Airline Names Shouldn't contain numbers or Special characters");
                return;

      
        }
         if (txtOwner.getText().equals("") || txtOwner.getText() == null) {
            JOptionPane.showMessageDialog(null, "Owner can't be empty");
            return;
        }
        
      
            if (!(txtOwner.getText().matches("[a-zA-Z][a-zA-Z ]+") )) {
                JOptionPane.showMessageDialog(null, "Owner Names Shouldn't contain numbers or Special characters");
                return;

      
        }
               if (txtContact.getText().equals("") || txtContact.getText() == null) {
            JOptionPane.showMessageDialog(null, "Contact can't be empty");
            return;
        }
               if (txtOfficeLoc.getText().equals("") || txtOfficeLoc.getText() == null) {
            JOptionPane.showMessageDialog(null, "Office Location can't be empty");
            return;
        }
               if (txtNoofFlights.getText().equals("") || txtNoofFlights.getText() == null) {
            JOptionPane.showMessageDialog(null, "No of flights can't be empty");
            return;
        }
           if (!(txtNoofFlights.getText().matches("[0-9]+"))) {
                JOptionPane.showMessageDialog(null, "Number of Flights should only contain numbers else Exception will be raised ");
               
                return;

            }
        
        
        int selectedRow = tblAirliner.getSelectedRow();
        if(selectedRow>=0){
            AirlinerUser airline = (AirlinerUser)tblAirliner.getValueAt(selectedRow, 0);
            airline.setName(txtAName.getText());
            airline.setContactInfo(txtContact.getText());
            airline.setNumberOfFlights(Integer.parseInt(txtNoofFlights.getText()));
            airline.setOfficeLocation(txtOfficeLoc.getText());
            airline.setOwner(txtOwner.getText());
            populateTable();
            JOptionPane.showMessageDialog(null,"Updated Successfully");
            txtAName.setText("");
            txtContact.setText("");
            txtNoofFlights.setText("");
            txtOfficeLoc.setText("");
            txtOwner.setText("");
            txtAName.setEnabled(false);
            txtContact.setEnabled(false);
            txtOfficeLoc.setEnabled(false);
            txtNoofFlights.setEnabled(false);
            txtOwner.setEnabled(false);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblAirliner.getSelectedRow();
        if(selectedRow>=0){
            AirlinerUser airline = (AirlinerUser)tblAirliner.getValueAt(selectedRow,0);
            txtAName.setText(airline.getName());
            txtContact.setText(airline.getContactInfo());
            String numberOfFlights =  Integer.toString(airline.getNumberOfFlights());
            txtNoofFlights.setText(numberOfFlights);
            txtOfficeLoc.setText(airline.getOfficeLocation());
            txtOwner.setText(airline.getOwner());
            
            txtAName.setEnabled(false);
            txtContact.setEnabled(false);
            txtNoofFlights.setEnabled(false);
            txtOfficeLoc.setEnabled(false);
            txtOwner.setEnabled(false);
        }
        else{
            JOptionPane.showMessageDialog(null,"Please select any row");
        }
    }//GEN-LAST:event_btnViewActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        cardSequenceJPanel.remove(this);
        CardLayout layout = (CardLayout) cardSequenceJPanel.getLayout();
        layout.previous(cardSequenceJPanel);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnView;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblAirliner;
    private javax.swing.JTextField txtAName;
    private javax.swing.JTextField txtContact;
    private javax.swing.JTextField txtNoofFlights;
    private javax.swing.JTextField txtOfficeLoc;
    private javax.swing.JTextField txtOwner;
    // End of variables declaration//GEN-END:variables
}
