/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Flight;
import Business.FlightDirectory;
import java.awt.CardLayout;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author gopikrishnamanchukonda
 */
public class ManageFlightSchedule extends javax.swing.JPanel {

    /**
     * Creates new form AirlinerFlightJPanel
     */
     private JPanel cardSequenceJPanel;
    private FlightDirectory flightDirectory;
//    private String userName;
//    private String passWord;
//    private AirlinerDirectory airlinerDirectory;
    private String airlineName;
    public ManageFlightSchedule() {
        initComponents();
    }
    
    public ManageFlightSchedule(JPanel cardSequenceJPanel, FlightDirectory flightDirectory, String airlineName) {
        initComponents();
         this.cardSequenceJPanel = cardSequenceJPanel;
          this.flightDirectory = flightDirectory;
          this.airlineName = airlineName;
          //this.userName = userName;
          //this.passWord = passWord;
          //this.airlinerDirectory = airlineDirectory;
          populateTable();
          txtAirline.setEnabled(false);
          txtDate.setEnabled(false);
          txtDept.setEnabled(false);
          txtSeats.setEnabled(false);
          txtStop.setEnabled(false);
          txtFno.setEnabled(false);
          txtPrice.setEnabled(false);
          txtStop.setEnabled(false);
          CheckStatus.setEnabled(false);
          txtArr.setEnabled(false);
          txtTime.setEnabled(false);
        
    }
    
    void populateTable(){
        DefaultTableModel dtm = (DefaultTableModel)tblFlights.getModel();
        dtm.setRowCount(0);
        
        for(Flight flight : flightDirectory.getFlightDirectory()){
            if(flight.getAirlineName().equals(airlineName)){
            Object[] row = new Object[6];
            row[0] = flight;
            row[1] = flight.getFlightNumber();
            row[2] = flight.getDeparture();
            row[3] = flight.getArrival();
            row[4] = flight.getDoj();
            row[5] = flight.getLocalTime2();
            dtm.addRow(row);
            
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblFlights = new javax.swing.JTable();
        btnUpdate = new javax.swing.JButton();
        btnView = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        btnConfirm = new javax.swing.JButton();
        txtAirline = new javax.swing.JTextField();
        txtFno = new javax.swing.JTextField();
        txtArr = new javax.swing.JTextField();
        txtDept = new javax.swing.JTextField();
        txtStop = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        txtTime = new javax.swing.JTextField();
        txtDate = new javax.swing.JTextField();
        txtSeats = new javax.swing.JTextField();
        CheckStatus = new javax.swing.JCheckBox();
        jLabel15 = new javax.swing.JLabel();

        tblFlights.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Airline", "FlightNumber", "From", "To", "Date", "time"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblFlights);

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        jLabel6.setText("Airline Name");

        jLabel7.setText("Flight Number");

        jLabel8.setText("Departure");

        jLabel9.setText("Arrival");

        jLabel10.setText("Stop");

        jLabel11.setText("Seats Avail");

        jLabel12.setText("price");

        jLabel13.setText("Date");

        jLabel14.setText("Time");

        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        CheckStatus.setText("Status");

        jLabel15.setText("Flight Schedule");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(317, 317, 317)
                        .addComponent(jLabel15))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel6)
                                .addComponent(jLabel7)
                                .addComponent(jLabel8)
                                .addComponent(jLabel9)
                                .addComponent(jLabel10))
                            .addGap(39, 39, 39)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(txtFno, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(52, 52, 52)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel12)
                                        .addComponent(jLabel13)
                                        .addComponent(jLabel14)
                                        .addComponent(jLabel11))
                                    .addGap(19, 19, 19)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtTime, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtDate, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtPrice, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtSeats, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(CheckStatus, javax.swing.GroupLayout.Alignment.LEADING)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtAirline, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtDept, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtArr, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtStop, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(165, 165, 165)
                                    .addComponent(btnConfirm))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(302, 302, 302)
                                .addComponent(btnAdd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnView)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnUpdate))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(143, 143, 143)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(186, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(50, 50, 50)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnView)
                    .addComponent(btnUpdate))
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSeats, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel13)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CheckStatus)
                        .addGap(18, 18, 18)
                        .addComponent(btnConfirm))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtAirline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtFno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(txtArr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txtDept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(txtStop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(80, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        
          txtAirline.setText("");
          txtDate.setText("");
          txtDept.setText("");
          txtSeats.setText("");
          txtStop.setText("");
          txtFno.setText("");
          txtPrice.setText("");
          txtStop.setText("");
          CheckStatus.setText("");
          txtArr.setText("");
          txtTime.setText("");
        
        txtAirline.setEnabled(true);
          txtDate.setEnabled(true);
          txtDept.setEnabled(true);
          txtSeats.setEnabled(true);
          txtStop.setEnabled(true);
          txtFno.setEnabled(true);
          txtPrice.setEnabled(true);
          txtStop.setEnabled(true);
          CheckStatus.setEnabled(true);
          txtArr.setEnabled(true);
          txtTime.setEnabled(true);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        // TODO add your handling code here:
          if (txtAirline.getText().equals("") || txtAirline.getText() == null) {
            JOptionPane.showMessageDialog(null, "Airliner can't be empty");
            return;
        }
        
      
            if (!(txtAirline.getText().matches("[a-zA-Z][a-zA-Z ]+") )) {
                JOptionPane.showMessageDialog(null, "Airline Names Shouldn't contain numbers or Special characters");
                return;

      
        }
        if (txtFno.getText().equals("") || txtFno.getText() == null) {
           
                JOptionPane.showMessageDialog(null, "Flight number Shouldn't be empty ");
                return;

            
        }
        if (txtDept.getText().equals("") || txtDept.getText() == null) {
            JOptionPane.showMessageDialog(null, "Departure can't be empty");
            return;
        }
        if (!(txtDept.getText().matches("[a-zA-Z]+"))){
            JOptionPane.showMessageDialog(null, "Departure Names Shouldn't contain numbers or Special characters");
            return;

        }
        if (txtArr.getText().equals("") || txtArr.getText() == null) {
            JOptionPane.showMessageDialog(null, "Arrivals can't be empty");
            return;
        }
        if (!(txtArr.getText().matches("[a-zA-Z]+"))){
            JOptionPane.showMessageDialog(null, "Arrivals Names Shouldn't contain numbers or Special characters");
            return;

        }
           if (!(txtStop.getText().matches("[a-zA-Z][a-zA-Z ]+"))){
            JOptionPane.showMessageDialog(null, "Stop Names Shouldn't contain numbers or Special characters");
            return;

        }
               if (txtPrice.getText().equals("") || txtPrice.getText() == null) {
            JOptionPane.showMessageDialog(null, "Price can't be empty");
            return;
        }
           if (!(txtPrice.getText().matches("[0-9]+"))) {
                JOptionPane.showMessageDialog(null, "Price should only contain numbers else Exception will be raised ");
               
                return;

            }
                if (txtSeats.getText().equals("") || txtSeats.getText() == null) {
            JOptionPane.showMessageDialog(null, "Seats Avalible  can't be empty");
            return;
        }
                  if (!(txtSeats.getText().matches("[0-9]+"))) {
                JOptionPane.showMessageDialog(null, "Price should only contain numbers else Exception will be raised ");
               
                return;

            }
                   if (txtDate.getText().equals("") || txtDate.getText() == null) {
            JOptionPane.showMessageDialog(null, "Date  can't be empty");
            return;
        }
        if (!txtDate.getText().matches("\\d{4}-\\d{2}-\\d{2}")) {
            JOptionPane.showMessageDialog(null, "Please Enter Date in YYYY-MM-DD format Only");
            return;
        } 
       if (txtTime.getText().equals("") || txtTime.getText() == null) {
            JOptionPane.showMessageDialog(null, "Time  can't be empty");
            return;
        }
         if (!txtTime.getText().matches("\\d{2}:\\d{2}:\\d{2}")) {
            JOptionPane.showMessageDialog(null, "Please Enter time in HH:MM:SS format Only");
            return;
        } 
        
        
        
        
        
        int selectedRow = tblFlights.getSelectedRow();
        if(selectedRow>=0){
            
            Flight flight = (Flight) tblFlights.getValueAt(selectedRow,0);
            if(CheckStatus.isSelected()== false){
             //   JOptionPane.showConfirmDialog(null,"Do you confirm to cancel the flight?");
             flightDirectory.removeFlight(flight);
             JOptionPane.showMessageDialog(null,"Flight cancelled Succesully");
             
            }
            flight.setAirlineName(txtAirline.getText());
            flight.setArrival(txtArr.getText());
            flight.setDeparture(txtDept.getText());
            flight.setFlightNumber(txtFno.getText());
            flight.setLayoverLocation(txtStop.getText());
            flight.setSeats(Integer.parseInt(txtSeats.getText()));
            flight.setPrice(Integer.parseInt(txtPrice.getText()));
            flight.setDoj(LocalDate.parse(txtDate.getText()));
            flight.setLocalTime2(LocalTime.now());
            populateTable();
            JOptionPane.showMessageDialog(null,"Updated Successfully");
        }
        else{
            Flight flight = new Flight();
        flight.setAirlineName(txtAirline.getText());
        flight.setArrival(txtArr.getText());
        flight.setDeparture(txtDept.getText());
        flight.setFlightNumber(txtFno.getText());
        flight.setLayoverLocation(txtStop.getText());
        flight.setSeats(Integer.parseInt(txtSeats.getText()));
        flight.setPrice(Integer.parseInt(txtPrice.getText()));
        flight.setDoj(LocalDate.parse(txtDate.getText()));
        flight.setLocalTime2(LocalTime.now());
        flight.setStatus(CheckStatus.isSelected());
        flightDirectory.addFlight(flight);
        JOptionPane.showMessageDialog(null,"Flight is added successfully");
        populateTable();
        }
        txtAirline.setEnabled(false);
          txtDate.setEnabled(false);
          txtDept.setEnabled(false);
          txtSeats.setEnabled(false);
          txtStop.setEnabled(false);
          txtFno.setEnabled(false);
          txtPrice.setEnabled(false);
          txtStop.setEnabled(false);
          CheckStatus.setEnabled(false);
          txtArr.setEnabled(false);
          txtTime.setEnabled(false);

    }//GEN-LAST:event_btnConfirmActionPerformed

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        // TODO add your handling code here:
        
        // TODO add your handling code here:
        int selectedRow = tblFlights.getSelectedRow();
        if(selectedRow>=0){
            Flight flight = (Flight)tblFlights.getValueAt(selectedRow,0);
           txtAirline.setText(flight.getAirlineName());
           txtFno.setText(flight.getFlightNumber());
           txtDate.setText(flight.getDoj().toString());
           txtDept.setText(flight.getDeparture());
           txtArr.setText(flight.getArrival());
           txtSeats.setText(Integer.toString(flight.getSeats()));
           txtPrice.setText(Integer.toString(flight.getPrice()));
           txtStop.setText(flight.getLayoverLocation());
           boolean status =flight.isStatus();
           CheckStatus.setSelected(status);
           
           
        }
        else{
        JOptionPane.showMessageDialog(null,"Please select a row");
        }
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        
         int selectedRow = tblFlights.getSelectedRow();
        if(selectedRow>=0){
            Flight flight =(Flight)tblFlights.getValueAt(selectedRow,0);
           txtAirline.setText(flight.getAirlineName());
           txtFno.setText(flight.getFlightNumber());
           txtDate.setText(flight.getDoj().toString());
           txtDept.setText(flight.getDeparture());
           txtArr.setText(flight.getArrival());
           txtSeats.setText(Integer.toString(flight.getSeats()));
           txtPrice.setText(Integer.toString(flight.getPrice()));
           txtStop.setText(flight.getLayoverLocation());
           txtTime.setText(flight.getLocalTime2().toString());
           boolean status =flight.isStatus();
           CheckStatus.setSelected(status);
           txtAirline.setEnabled(true);
          txtDate.setEnabled(true);
          txtDept.setEnabled(true);
          txtSeats.setEnabled(true);
          txtStop.setEnabled(true);
          txtFno.setEnabled(true);
          txtPrice.setEnabled(true);
          txtStop.setEnabled(true);
          CheckStatus.setEnabled(true);
          txtArr.setEnabled(true);
          txtTime.setEnabled(true);
        }
        else{
        JOptionPane.showMessageDialog(null,"Please select a row");
        }
        
    }//GEN-LAST:event_btnUpdateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckStatus;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnView;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblFlights;
    private javax.swing.JTextField txtAirline;
    private javax.swing.JTextField txtArr;
    private javax.swing.JTextField txtDate;
    private javax.swing.JTextField txtDept;
    private javax.swing.JTextField txtFno;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtSeats;
    private javax.swing.JTextField txtStop;
    private javax.swing.JTextField txtTime;
    // End of variables declaration//GEN-END:variables
}
