/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

public class AnalysisHelper {
    // find user with Most Likes
    // TODO
      List <User> sortedUserList = new ArrayList<User>();
     public void userWithMostLikes(){
        Map<Integer,Integer> userLikecount = new HashMap<Integer, Integer>();
        
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        for(User user : users.values()){
            for(Comment c: user.getComments()){
            int likes = 0;
            if(userLikecount.containsKey(user.getId()))
                    likes = userLikecount.get(user.getId());
            likes+=c.getLikes();
            userLikecount.put(user.getId(),likes);
            }
        }
                int max = 0;
                int maxId =0;
                for(int id: userLikecount.keySet()){
                    if(userLikecount.get(id)>max){
                        max=userLikecount.get(id);
                            maxId=id;
                            }
                }
                System.out.println("\nUser with most likes: "+max+"\n"+users.get(maxId));
    }
    public void getFiveMostLikedComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>(){
       
        @Override
        public int compare(Comment o1, Comment o2){
        return o2.getLikes()-o1.getLikes();
        }
        });
        
        System.out.println("\npost with most liked comments:");
        for (int i=0; i< commentList.size()&&i<5;i++){
            System.out.println(commentList.get(i));
            //break;
        }
    }
    
    public void averageLikesPerComment() {
        Map<Integer, Comment> Comments = DataStore.getInstance().getComments();

        int totalComments = 0;
        float totalLikes = 0;

        for (Comment c : Comments.values()) {
            totalComments++;
            totalLikes = totalLikes + c.getLikes();
        }
        float average = totalLikes / totalComments;
        System.out.println('\n');
        System.out.println ("total likes:" + totalLikes);
        System.out.println ("total comments:" + totalComments);
        System.out.println("1. Average number of likes per comment: " + average + '\n' );
        
    }
    
    public void postWithMostLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        // Key: postId  Value: number of likes of the most liked comment under the post
        Map<Integer, Integer> PostWithMostLikes = new HashMap<>();

        Iterator<Comment> it = comments.values().iterator();
        while (it.hasNext()) {
            Comment next = it.next();
            int postId = next.getPostId();
            int likes = next.getLikes();
            if (PostWithMostLikes.containsKey(postId)) {
                int currentMax = PostWithMostLikes.get(postId);
                if (likes > currentMax) {
                    PostWithMostLikes.remove(postId);
                    PostWithMostLikes.put(postId, likes);
                }
            } else {
                PostWithMostLikes.put(postId, likes);
            }
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(PostWithMostLikes.entrySet());
        Collections.sort(list, descendingComp);

        System.out.println('\n');
        System.out.println("******************2. Post with most liked comments: ******************");

        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                int postId = list.get(i).getKey();
                Post post = getPostById(postId);
                System.out.println("Post Id: " + postId);
                System.out.println("Likes Number: " + list.get(i).getValue());
                System.out.println("Posting User Id: " + post.getUserId());
                System.out.println("Contains comments: ");
                for (Comment c : post.getComments()) {
                    System.out.println("- Comment-id " + c.getId()+";" + " likes: " + c.getLikes()+";" + " comment: " + c.getText());
                }
            } else {
                if (list.get(i).getValue().equals(list.get(0).getValue())) {;
                    System.out.println(" The following Post has the same most liked comments ");
                    int postId = list.get(i).getKey();
                    Post post = getPostById(postId);
                    System.out.println("Post Id: " + postId);
                    System.out.println("Likes Number: " + list.get(i).getValue());
                    System.out.println("Posting User Id: " + post.getUserId());
                    System.out.println("Contains comments: ");
                    for (Comment c : post.getComments()) {
                         System.out.println("- Comment-id " + c.getId()+";" + " likes: " + c.getLikes()+";" + " comment: " + c.getText());
                    }
                } else {
                    break;
                }
            }
        }
    }
    
     private Post getPostById(int id) {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        if (posts.containsKey(id)) {
            return posts.get(id);
        } else {
            return null;
        }
    }
    // sort with descending order 
    Comparator<Map.Entry<Integer, Integer>> descendingComp = new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o2.getValue() - o1.getValue();
        }
    };
   // sort with ascending order 
    Comparator<Map.Entry<Integer, Integer>> ascendingComp = new Comparator<Map.Entry<Integer, Integer>>() {
        @Override
        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
            return o1.getValue() - o2.getValue();
        }
    };
    
    public void postWithMostComments() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        // Key: Post Id  Value: total_comments under the post
        Map<Integer, Integer> totalComments = new HashMap<>();

        Iterator<Map.Entry<Integer, Post>> it = posts.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<Integer, Post> next = it.next();
            int postId = next.getKey();
            Post post = next.getValue();

            totalComments.put(postId, post.getComments().size());
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(totalComments.entrySet());
        Collections.sort(list, descendingComp);

        System.out.println('\n');
        System.out.println("******************3. Post with most comments: ******************");
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                int postId = list.get(i).getKey();
                Post post = getPostById(postId);
                System.out.println("Post Id: " + postId);
                System.out.println("Posting User Id: " + post.getUserId());
                System.out.println("Contains " + list.get(i).getValue() + " comments: ");
                for (Comment comment : post.getComments()) {
                     System.out.println("- Comment-id " + comment.getId()+";" + " likes: " + comment.getLikes()+";" + " comment: " + comment.getText());
                }
            } else {
                if (list.get(i).getValue().equals(list.get(0).getValue())) {
                    int postId = list.get(i).getKey();
                    Post post = getPostById(postId);
                    System.out.println("The following Post has the same most comments: ");
                    System.out.println("Post Id: " + post.getPostId());
                    System.out.println("Posting User Id: " + post.getUserId());
                    System.out.println("Contains " + list.get(i).getValue() + " comments: ");
                    for (Comment comment : post.getComments()) {
                        System.out.println("- Comment-id " + comment.getId()+";" + " likes: " + comment.getLikes()+";" + " comment: " + comment.getText());
                    }
                }
            }
        }
    }
    
    public void getTop5InactiveUsersBasedOnPost() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> activity = new HashMap<Integer, Integer>();
        //List<User> userList = new ArrayList<User>(users.values());
         Map<Integer, Integer> postCount = new HashMap<Integer, Integer>();
        int psum=0;
        for (User user : users.values()) {
        for (Post post : posts.values()) {
            
            if (post.getUserId() == user.getId()) {
                psum++;
            }
            postCount.put(user.getId(), psum);
        }
       }        
        List <Integer> pactivity = new ArrayList<Integer>(postCount.values());
        Collections.sort(pactivity,new Comparator<Integer>(){

            @Override
            public int compare(Integer o1, Integer o2) {
              return o1-o2; //To change body of generated methods, choose Tools | Templates.
            }
    }
    );
        System.out.println("pactivity");
        System.out.println(pactivity);
           System.out.println("postCount");
        System.out.println(postCount);
        List <User> postsortedUserList = new ArrayList<User>();
        List<User> userList = new ArrayList<>(users.values());
               for(int i=0;i<pactivity.size();i++){
               for(User u:users.values()){
                   //actList.get(i)==activity.get(u.getId()
         
               if(pactivity.get(i)==postCount.get(u.getId())){
                   
                   postsortedUserList.add(u);   
               }   
       }
       }
               System.out.println("List of 5 inactive users based on the posts"); 
         for(int i =0;i<5;i++){
             System.out.println(postsortedUserList.get(i));
         }
    }
     
     public void getTop5InactiveUsersBasedOnComments(){
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        
        List<User> userList = new ArrayList<>(users.values());
        
        Collections.sort(userList, new Comparator<User>(){
       
            
        @Override
        public int compare(User o1, User o2){
            
        return o2.getComments().size() - o1.getComments().size();
        }
        });
        
        System.out.println("\n5 most inactive User based on comments:");
        for (int i=0; i< userList.size()&&i<5;i++){
            System.out.println(userList.get(i));
        }
    }

    
    // find 5 comments which have the most likes
    // TODO

     public void getTop5InactiveUsers() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        int tsum = 0, psum = 0, csum = 0;
        Map<Integer, Integer> cmtCount = new HashMap<Integer, Integer>();
        Map<Integer, Integer> postCount = new HashMap<Integer, Integer>();
        //Map<Integer, Integer> likeCount = new HashMap<Integer, Integer>();
        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>();
         Map<Integer, Integer> activity = new HashMap<Integer, Integer>();
           Map<Integer, Integer> sortedActivity = new HashMap<Integer, Integer>();
            Map<User, Integer> result = new HashMap<User, Integer>();
           
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                if (c.getUserId() == user.getId()) {
                    csum++;
                }
                cmtCount.put(user.getId(), csum);
                int likes = 0;
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            

        }
        for (Post post : posts.values()) {
            if (post.getUserId() == user.getId()) {
                psum++;
            }
            postCount.put(user.getId(), psum);
        }

    }
      for(User u:users.values()){
          int sum=0;
          sum = cmtCount.get(u.getId())+postCount.get(u.getId())+userLikecount.get(u.getId());
          activity.put(u.getId(),sum);
      }
//        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
//        List<Comment> commentList = new ArrayList<>(comments.values());
//        
//        Collections.sort(commentList,new Comparator<Comment>(){
//            @Override
//            public int compare(Comment o1, Comment o2){
//                return o2.getLikes()-o1.getLikes();
//            }
//        });
//        System.out.println("5 most liked comments :");
//        for(int i=0;i<commentList.size()&& i<5;i++){
//            System.out.println(commentList.get(i));
//        }
    List <Integer> actList = new ArrayList<>(activity.values());
    
    Collections.sort(actList,new Comparator<Integer>(){


            @Override
            public int compare(Integer o1, Integer o2) {
              return o1-o2; //To change body of generated methods, choose Tools | Templates.
            }
    }
    
    
    );
    System.out.println(activity);
    // Map<Integer, User> users = DataStore.getInstance().getUsers();

    List<User> userList = new ArrayList<>(users.values());
    //List<User> sortedUserList = new ArrayList<>();
      
           for(int i=0;i<actList.size();i++){
               for(User u:users.values()){
               if(actList.get(i)==activity.get(u.getId())){
                   sortedUserList.add(u);
               }
             
           
       }
       }
       System.out.println("List of top 5 In active users");
       System.out.println(actList);
       for(int i=0;i<sortedUserList.size()&&i<5;i++){
           System.out.println(sortedUserList.get(i));
       } 
       


  


   
//       List<Integer> mapvalues = new ArrayList<>(activity.values());
//       Collections.sort(mapvalues);
//       Iterator<Integer> valueIt = mapvalues.iterator();
//       
//       while(valueIt.hasNext()){
//           int val = valueIt.next();
//           if(val==activity.get)
//       }
       
      
     // Map<Integer, Integer> sortedMapAsc = sortByComparator(activity, true);
     
      
        

}
     public void getTop5ProactiveUsers(){
        Collections.reverse(sortedUserList);
        System.out.println("List of pro active users");
         for(int i=0;i<sortedUserList.size()&&i<5;i++){
           System.out.println(sortedUserList.get(i));
       } 
    }
}
